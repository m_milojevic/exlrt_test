<?php
/**
 * Created by Milos.
 * User: Milos Milojevic (milosmoto@gmail.com)
 * Date: 15.4.16.
 * Time: 09.20
 */

namespace AppBundle\Tests\Controller\API;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BookControllerTest extends WebTestCase
{
    public function testList()
    {
        $client = static::createClient();

        $client->request('GET', '/api/books/highlighted.json');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testSingle()
    {
        $client = static::createClient();

        $client->request('GET', '/api/books/1.json');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testSearch()
    {
        $client = static::createClient();

        $client->request('GET', '/api/books/test/search.json');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}