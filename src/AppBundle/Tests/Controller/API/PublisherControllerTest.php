<?php
/**
 * Created by Milos.
 * User: Milos Milojevic (milosmoto@gmail.com)
 * Date: 15.4.16.
 * Time: 09.20
 */

namespace AppBundle\Tests\Controller\API;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PublisherControllerTest extends WebTestCase
{
    public function testList()
    {
        $client = static::createClient();

        $client->request('GET', '/api/publishers.json');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testSingle()
    {
        $client = static::createClient();

        $client->request('GET', '/api/publishers/1n.json');

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }
}