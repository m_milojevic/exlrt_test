<?php
/**
 * Created by Milos.
 * User: Milos Milojevic (milosmoto@gmail.com)
 * Date: 15.4.16.
 * Time: 09.27
 */

namespace AppBundle\Tests\Controller;

use AppBundle\Entity\User;

class UserTest extends \PHPUnit_Framework_TestCase
{
    public function testGettersAndSetters()
    {
        $user = new User();

        $user->setType(User::ROLE_AUTHOR);
        $this->assertEquals(User::ROLE_AUTHOR, $user->getType());
    }
}