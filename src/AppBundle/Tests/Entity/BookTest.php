<?php
/**
 * Created by Milos.
 * User: Milos Milojevic (milosmoto@gmail.com)
 * Date: 15.4.16.
 * Time: 09.12
 */

namespace AppBundle\Tests\Controller;

use AppBundle\Entity\Book;
use AppBundle\Entity\User;

class BookTest extends \PHPUnit_Framework_TestCase
{
    public function testGettersAndSetters()
    {
        $book = new Book();

        $book->setName('test');
        $this->assertEquals('test', $book->getName());

        $book->setDescription('test');
        $this->assertEquals('test', $book->getDescription());

        $user = new User();
        $user->setEmail('test@test.com');

        $book->setAuthor($user);
        $this->assertEquals('test@test.com', $book->getAuthor()->getEmail());

        $book->addPublisher($user);
        $this->assertEquals('test@test.com', $book->getPublishers()->first()->getEmail());
    }
}
