<?php
/**
 * Created by Milos Milojevic(milosmoto@gmail.com).
 * User: milos
 * Date: 14.4.16.
 * Time: 17.32
 */

namespace AppBundle\Entity;

/**
 *
 */
interface BuilderInterface
{
    /**
     * @return mixed
     */
    public function getMapped();
}