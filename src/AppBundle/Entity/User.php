<?php
/**
 * Created by Milos.
 * User: Milos Milojevic (milosmoto@gmail.com)
 * Date: 13.4.16.
 * Time: 09.20
 */

namespace AppBundle\Entity;

use AppBundle\Entity\BuilderInterface;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User extends BaseUser implements BuilderInterface
{
    const ROLE_PUBLISHER = 'ROLE_PUBLISHER';
    const ROLE_AUTHOR = 'ROLE_AUTHOR';

    public function getMapped()
    {
        return array(
            'id' => $this->id,
            'email' => $this->email,
            'username' => $this->username
        );
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $type;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return User
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }
}