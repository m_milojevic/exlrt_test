<?php
/**
 * Created by Milos.
 * User: Milos Milojevic (milosmoto@gmail.com)
 * Date: 13.4.16.
 * Time: 09.20
 */

namespace AppBundle\Entity;

use AppBundle\Entity\BuilderInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="book")
 */
class Book implements BuilderInterface
{
    const TYPE_FEATURED = 'featured';
    const TYPE_HIGHLIGHTED = 'highlighted';

    public function getMapped()
    {
        return array(
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description
        );
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    protected $type = self::TYPE_HIGHLIGHTED;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    protected $author;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinTable(name="book_publisher")
     */
    private $publishers;

    public function __construct(){
        $this->publishers = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Book
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return Book
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     * @return Book
     */
    public function setAuthor(User $author)
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getPublishers()
    {
        return $this->publishers;
    }

    /**
     * @param ArrayCollection $publishers
     * @return Book
     */
    public function setPublishers(ArrayCollection $publishers)
    {
        $this->publishers = $publishers;
        return $this;
    }

    /**
     * Add publisher
     *
     * @param \AppBundle\Entity\User $publisher
     * @return Book
     */
    public function addPublisher(User $publisher)
    {
        $this->publishers->add($publisher);
        return $this;
    }

    /**
     * Remove publisher
     *
     * @param \AppBundle\Entity\User $publisher
     * @return Book
     */
    public function removePublisher(User $publisher)
    {
        $this->publishers->removeElement($publisher) ;
        return $this;
    }
}