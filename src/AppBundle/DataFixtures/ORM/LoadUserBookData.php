<?php
/**
 * Created by Milos.
 * User: Milos Milojevic (milosmoto@gmail.com)
 * Date: 13.4.16.
 * Time: 14.34
 */

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Book;
use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUserBookData implements FixtureInterface
{

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $publisher1 = new User();
        $publisher1->setEmail('milosmoto+publisher1@gmail.com')
            ->setUsername('publisher1')
            ->setPlainPassword('milosmoto')
            ->setEnabled(true)
            ->setType(User::ROLE_PUBLISHER)
            ->addRole(User::ROLE_PUBLISHER);

        $manager->persist($publisher1);

        $publisher2 = new User();
        $publisher2->setEmail('milosmoto+publisher2@gmail.com')
            ->setUsername('publisher2')
            ->setPlainPassword('milosmoto')
            ->setEnabled(true)
            ->setType(User::ROLE_PUBLISHER)
            ->addRole(User::ROLE_PUBLISHER);

        $manager->persist($publisher2);

        $author = new User();
        $author->setEmail('milosmoto+author@gmail.com')
            ->setUsername('author')
            ->setPlainPassword('milosmoto')
            ->setEnabled(true)
            ->setType(User::ROLE_AUTHOR)
            ->addRole(User::ROLE_AUTHOR);

        $manager->persist($author);

        $manager->flush();

        $book1 = new Book();
        $book1->setAuthor($author)
                ->setDescription('Great book.')
                ->setName('The da vinci code')
                ->addPublisher($publisher1)
                ->addPublisher($publisher2);

        $manager->persist($book1);

        $book2 = new Book();
        $book2->setAuthor($author)
            ->setDescription('Great book from Den Brown.')
            ->setName('Lost symbols')
            ->addPublisher($publisher1)
            ->addPublisher($publisher2);


        $manager->persist($book2);

        $manager->flush();
    }
}