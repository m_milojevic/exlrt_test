<?php
/**
 * Created by Milos Milojevic(milosmoto@gmail.com).
 * User: milos
 * Date: 14.4.16.
 * Time: 17.49
 */

namespace AppBundle\Service;

use AppBundle\Entity\User;

class PublisherFactory extends AbstractFactory
{

    public function getList($page, $limit, $type = false){
        $query = $this->em->getRepository('AppBundle:User')
            ->createQueryBuilder('u')
            ->where('u.type = :type')
            ->setParameter('type', User::ROLE_PUBLISHER)
            ->getQuery();

        $pagination = $this->paginator->paginate(
            $query,
            $page,
            $limit
        );

        $publishersResponse = array();

        foreach($pagination as $user){
            $publishersResponse[] = $user->getMapped();
        }

        return $publishersResponse;
    }

    public function getSingle($id){
        $publisher = $this->em->getRepository('AppBundle:User')->findOneBy(array('id' => $id, 'type' => User::ROLE_PUBLISHER));
        if($publisher){
            return $publisher->getMapped();
        }else
            return false;
    }
}
