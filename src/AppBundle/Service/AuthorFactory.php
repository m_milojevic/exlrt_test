<?php
/**
 * Created by Milos Milojevic(milosmoto@gmail.com).
 * User: milos
 * Date: 14.4.16.
 * Time: 17.48
 */

namespace AppBundle\Service;

use AppBundle\Entity\User;

class AuthorFactory extends AbstractFactory
{

    public function getList($page, $limit, $type = false){
        $query = $this->em->getRepository('AppBundle:User')
            ->createQueryBuilder('u')
            ->where('u.type = :type')
            ->setParameter('type', User::ROLE_AUTHOR)
            ->getQuery();

        $pagination = $this->paginator->paginate(
            $query,
            $page,
            $limit
        );

        $authorsResponse = array();

        foreach($pagination as $user){
            $authorsResponse[] = $user->getMapped();
        }

        return $authorsResponse;
    }

    public function getSingle($id){
        $author = $this->em->getRepository('AppBundle:User')->findOneBy(array('id' => $id, 'type' => User::ROLE_AUTHOR));
        if($author){
            return $author->getMapped();
        }else
            return false;
    }
}
