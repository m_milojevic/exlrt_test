<?php
/**
 * Created by Milos Milojevic(milosmoto@gmail.com).
 * User: milos
 * Date: 14.4.16.
 * Time: 17.34
 */

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use Knp\Component\Pager\Paginator;

abstract class AbstractFactory
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Paginator
     */
    protected $paginator;

    /**
     * @param Doctrine\ORM\EntityManager
     * @param Knp\Component\Pager\Paginator
     */
    public function __construct(EntityManager $em, Paginator $paginator){
        $this->em = $em;
        $this->paginator = $paginator;
    }

    /**
     * @param $page
     * @param $limit
     * @param $type
     * @return mixed
     */
    abstract public function getList($page, $limit, $type);

    /**
     * @param $id
     * @return mixed
     */
    abstract public function getSingle($id);

    /**
     * @param $keyword
     * @return array
     */
    public function getSearch($keyword, $page, $limit){
        return array();
    }
}
