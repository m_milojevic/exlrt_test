<?php
/**
 * Created by Milos Milojevic(milosmoto@gmail.com).
 * User: milos
 * Date: 14.4.16.
 * Time: 17.50
 */

namespace AppBundle\Service;

class BookFactory extends AbstractFactory
{

    public function getList($page, $limit, $type){
        $query = $this->em->getRepository('AppBundle:Book')
            ->createQueryBuilder('b')
            ->where('b.type = :type')
            ->setParameter('type', $type)
            ->getQuery();

        $pagination = $this->paginator->paginate(
            $query,
            $page,
            $limit
        );

        $booksResponse = array();

        foreach($pagination as $book){
            $booksResponse[] = $book->getMapped();
        }

        return $booksResponse;
    }

    public function getSingle($id){
        $book = $this->em->getRepository('AppBundle:Book')->findOneBy(array('id' => $id));
        if($book){
            return $book->getMapped();
        }else
            return false;
    }

    public function getSearch($page, $limit, $keyword){
        $query = $this->em->getRepository('AppBundle:Book')
            ->createQueryBuilder('b')
            ->where('b.name LIKE :keyword')
            ->setParameter('keyword', "%" . $keyword . "%")
            ->getQuery();

        $pagination = $this->paginator->paginate(
            $query,
            $page,
            $limit
        );

        $booksResponse = array();

        foreach($pagination as $book){
            $booksResponse[] = $book->getMapped();
        }

        return $booksResponse;
    }
}
