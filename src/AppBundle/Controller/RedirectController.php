<?php
/**
 * Created by Milos Milojevic(milosmoto@gmail.com).
 * User: milos
 * Date: 14.4.16.
 * Time: 18.59
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class RedirectController extends Controller
{

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request){
        return $this->redirectToRoute('nelmio_api_doc_index');
    }
}