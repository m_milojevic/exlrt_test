<?php
/**
 * Created by EtonDigital.
 * User: Milos Milojevic (milos.milojevic@etondigital.com)
 * Date: 13.4.16.
 * Time: 10.34
 */

namespace AppBundle\Controller\API;

use AppBundle\Entity\Book;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\HttpFoundation\Request;

class BookController extends FOSRestController
{
    /**
     * Get book list
     *
     * @ApiDoc(
     *   resource = true,
     *   section = "Books",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Not found resource",
     *   },
     *   requirements={
     *      {
     *          "name"="type",
     *          "dataType"="string",
     *          "requirement"="\d+",
     *          "description"="Book type, default='highlighted'"
     *      },
     *      {
     *          "name"="page",
     *          "dataType"="string",
     *          "requirement"="\d+",
     *          "description"="Pagination page"
     *      },
     *      {
     *          "name"="limit",
     *          "dataType"="string",
     *          "requirement"="\d+",
     *          "description"="Pagination offset"
     *      }
     *   }
     * )
     *
     * @Route(requirements={"_format"="json"})
     * @param Request $request the request object
     *
     * @return json
     *
     */
    public function getBooksAction(Request $request, $type = Book::TYPE_HIGHLIGHTED)
    {
        $factory = $this->get('book_factory');
        $response = $factory->getList($request->get('page', 1), $request->get('limit', 5), $type);

        $view = View::create()
            ->setStatusCode(Codes::HTTP_OK)
            ->setData($response);

        return $this->get('fos_rest.view_handler')->handle($view);
    }

    /**
     * Get single book
     *
     * @ApiDoc(
     *   resource = true,
     *   section = "Books",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Not found resource",
     *   },
     *   requirements={
     *      {
     *          "name"="id",
     *          "dataType"="string",
     *          "requirement"="\d+",
     *          "description"="Book id"
     *      }
     *   }
     * )
     *
     * @Route(requirements={"_format"="json"})
     * @param Request $request the request object
     *
     * @return json
     *
     */
    public function getBookAction(Request $request, $id)
    {
        $factory = $this->get('book_factory');
        $response = $factory->getSingle($id);

        if(!$response){
            $view = View::create()
                ->setStatusCode(Codes::HTTP_BAD_REQUEST)
                ->setData(array('message' => 'Not found resource.'));

            return $this->get('fos_rest.view_handler')->handle($view);
        }

        $view = View::create()
            ->setStatusCode(Codes::HTTP_OK)
            ->setData($response);

        return $this->get('fos_rest.view_handler')->handle($view);
    }

    /**
     * Search books by keyword
     *
     * @ApiDoc(
     *   resource = true,
     *   section = "Books",
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   },
     *   requirements={
     *      {
     *          "name"="keyword",
     *          "dataType"="string",
     *          "requirement"="\d+",
     *          "description"="Book keyword"
     *      },
     *      {
     *          "name"="page",
     *          "dataType"="string",
     *          "requirement"="\d+",
     *          "description"="Pagination page"
     *      },
     *      {
     *          "name"="limit",
     *          "dataType"="string",
     *          "requirement"="\d+",
     *          "description"="Pagination offset"
     *      }
     *   }
     * )
     *
     * @Route(requirements={"_format"="json"})
     * @param Request $request the request object
     *
     * @return json
     *
     */
    public function getBooksSearchAction(Request $request, $keyword)
    {
        $factory = $this->get('book_factory');
        $response = $factory->getSearch($request->get('page', 1), $request->get('limit', 5), $keyword);

        $view = View::create()
            ->setStatusCode(Codes::HTTP_OK)
            ->setData($response);

        return $this->get('fos_rest.view_handler')->handle($view);
    }
}