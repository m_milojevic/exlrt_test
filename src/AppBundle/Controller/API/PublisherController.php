<?php
/**
 * Created by Milos.
 * User: Milos Milojevic (milosmoto@gmail.com)
 * Date: 13.4.16.
 * Time: 10.28
 */

namespace AppBundle\Controller\API;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\HttpFoundation\Request;

class PublisherController extends FOSRestController
{
    /**
     * Get publisher list
     *
     * @ApiDoc(
     *   resource = true,
     *   section = "Publishers",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Not found resource",
     *   },
     *   requirements={
     *      {
     *          "name"="page",
     *          "dataType"="string",
     *          "requirement"="\d+",
     *          "description"="Pagination page"
     *      },
     *      {
     *          "name"="limit",
     *          "dataType"="string",
     *          "requirement"="\d+",
     *          "description"="Pagination offset"
     *      }
     *   }
     * )
     *
     * @Route(requirements={"_format"="json"})
     * @param Request $request the request object
     *
     * @return json
     *
     */
    public function getPublishersAction(Request $request)
    {
        $factory = $this->get('publisher_factory');
        $response = $factory->getList($request->get('page', 1), $request->get('limit', 5));

        $view = View::create()
            ->setStatusCode(Codes::HTTP_OK)
            ->setData($response);

        return $this->get('fos_rest.view_handler')->handle($view);
    }

    /**
     * Get single publisher
     *
     * @ApiDoc(
     *   resource = true,
     *   section = "Publishers",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Not found resource",
     *   },
     *   requirements={
     *      {
     *          "name"="id",
     *          "dataType"="string",
     *          "requirement"="\d+",
     *          "description"="Book id"
     *      }
     *   }
     * )
     *
     * @Route(requirements={"_format"="json"})
     * @param Request $request the request object
     *
     * @return json
     *
     */
    public function getPublisherAction(Request $request, $id)
    {
        $factory = $this->get('publisher_factory');
        $response = $factory->getSingle($id);

        if(!$response){
            $view = View::create()
                ->setStatusCode(Codes::HTTP_BAD_REQUEST)
                ->setData(array('message' => 'Not found resource.'));

            return $this->get('fos_rest.view_handler')->handle($view);
        }

        $view = View::create()
            ->setStatusCode(Codes::HTTP_OK)
            ->setData($response);

        return $this->get('fos_rest.view_handler')->handle($view);
    }
}
