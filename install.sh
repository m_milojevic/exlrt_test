#!/bin/bash
# Symfony2 clear cache, should be chmod 755  before run
app/console ca:cl --env=prod
app/console doc:mig:diff
yes | app/console doc:mig:mig
app/console as:in --symlink
chmod 777 -R app/cache
exit
