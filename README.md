exlrt_test
==========

A Symfony project created on April 13, 2016, 9:07 am.


Installation steps:

1) Install composer
2) From project root run:  composer install
    Accept you mysql username and password and database name for this project, default is exlrt_test.
3)Create empty database named in composer install step.
4) From project root run: sh install.sh

Front controller is web/app.php
Development front controller is web/app_dev.php


PhpUnit testing start from root:  phpunit -c app/